#include "queue_lab2_part2.h"
#include "unity.h"
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#define QUEUE_SIZE 128

void test_comprehensive_p2(void) {

  static uint8_t memory[QUEUE_SIZE]; // Change if needed
  queue_s queue;
  queue__init(&queue, memory, sizeof(memory));

  // push elements
  for (size_t item = 0; item < QUEUE_SIZE; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }
  // check if queue is full after push
  TEST_ASSERT_FALSE(queue__push(&queue, 123));
  TEST_ASSERT_EQUAL(QUEUE_SIZE, queue__get_item_count(&queue));

  // pop hlaf elements after queue is full
  for (size_t item = 0; item < (QUEUE_SIZE / 2); item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
  }

  // push another half because we popped those half, queue is now full again
  for (size_t item = 0; item < (QUEUE_SIZE / 2); item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    // TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }

  // queue is full
  TEST_ASSERT_FALSE(queue__push(&queue, 123));
  TEST_ASSERT_EQUAL(QUEUE_SIZE, queue__get_item_count(&queue));

  // empty the entire queue, pop 100 elements
  for (size_t item = 0; item < QUEUE_SIZE; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
  }

  // queue is already empty
  uint8_t popped_value = 0;
  TEST_ASSERT_FALSE(queue__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(0, queue__get_item_count(&queue));

  // push and pop one element
  const uint8_t pushed_value = 0x1A;
  TEST_ASSERT_TRUE(queue__push(&queue, pushed_value));
  TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);
}