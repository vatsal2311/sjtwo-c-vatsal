#include "unity.h"
#include <stdlib.h>
#include <string.h>
// Mocks
#include "Mockclock.h"
#include "Mockuart.h"

#include "Mockqueue.h"

// We can choose to use real implementation (not Mock) for line_buffer.h
// because this is a relatively trivial module
#include "line_buffer.h"

// Include the source we wish to test
#include "gps.h"

void setUp(void) {}
void tearDown(void) {}

void test_init(void) {
  const uint32_t expected_baud_rate = 38400;
  const uint32_t peripheral_clock_hz = 12000000;

  clock__get_peripheral_clock_hz_ExpectAndReturn(peripheral_clock_hz);

  uart__init_Expect(UART__3, peripheral_clock_hz, expected_baud_rate);

  QueueHandle_t expected_rx_queue = (QueueHandle_t)1; // Mocked pointer value
  QueueHandle_t expected_tx_queue = (QueueHandle_t)2; // Mocked pointer value
  xQueueCreate_ExpectAndReturn(50, sizeof(char), expected_rx_queue);
  xQueueCreate_ExpectAndReturn(8, sizeof(char), expected_tx_queue);

  uart__enable_queues_ExpectAndReturn(UART__3, expected_rx_queue, expected_tx_queue, true);

  gps__init();
}

void test_GPGLL_line_is_ignored(void) {

  const char *uart_driver_returned_data = "$GPGLL,lll.ll,a,yyyyy.yy,a,hhmmss.ss,A llll.ll\n";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    char the_char_to_return = uart_driver_returned_data[index];

    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, &the_char_to_return, 0, last_char);
    // TODO: Research on ReturnThruPtr() to make it return the char 'the_char_to_return'
    uart__get_ReturnThruPtr_input_byte(&the_char_to_return);
  }
  gps__run_once();
  // gps__get_coordinates();
  gps_coordinates_t coordinates = gps__get_coordinates();
  TEST_ASSERT_EQUAL_FLOAT(0.0, coordinates.latitude);
  TEST_ASSERT_EQUAL_FLOAT(0.0, coordinates.longitude);
}

void test_GPGGA_coordinates_are_parsed(void) {

  const char *uart_driver_returned_data = "$GPGGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh\r\n";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    char the_char_to_return = uart_driver_returned_data[index];

    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, &the_char_to_return, 0, last_char);
    // TODO: Research on ReturnThruPtr() to make it return the char 'the_char_to_return'
    uart__get_ReturnThruPtr_input_byte(&the_char_to_return);
  }
  gps__run_once();

  // TODO: Test gps__get_coordinates():
  gps_coordinates_t coordinates = gps__get_coordinates();
  TEST_ASSERT_EQUAL_FLOAT(atof("llll.ll") / 100.0, coordinates.latitude);
  TEST_ASSERT_EQUAL_FLOAT(atof("yyyyy.yy") / 100.0, coordinates.longitude);
}

void test_GPGGA_incomplete_line(void) {

  const char *uart_driver_returned_data = "$GPGGA,hhmmss.ss";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    char the_char_to_return = uart_driver_returned_data[index];

    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, &the_char_to_return, 0, last_char);
    // TODO: Research on ReturnThruPtr() to make it return the char 'the_char_to_return'
    uart__get_ReturnThruPtr_input_byte(&the_char_to_return);
  }
  gps__run_once();
  gps_coordinates_t coordinates = gps__get_coordinates();
  TEST_ASSERT_EQUAL_FLOAT(0.0, coordinates.latitude);
  TEST_ASSERT_EQUAL_FLOAT(0.0, coordinates.longitude);
}
