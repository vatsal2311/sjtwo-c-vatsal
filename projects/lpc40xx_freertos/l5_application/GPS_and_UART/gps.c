#include "gps.h"
#include <stdio.h>
#include <string.h>
// GPS module dependency
#include "line_buffer.h"
#include "uart.h"

#include "clock.h" // needed for UART initialization

// Change this according to which UART you plan to use
static const uart_e gps_uart = UART__3;

// Space for the line buffer, and the line buffer data structure instance
static char line_buffer[200];
static line_buffer_s line;

static gps_coordinates_t parsed_coordinates;

static void gps__transfer_data_from_uart_driver_to_line_buffer(void) {
  char byte;
  const uint32_t zero_timeout = 0;

  while (uart__get(gps_uart, &byte, zero_timeout)) {
    line_buffer__add_byte(&line, byte);
  }
}

static void gps__parse_coordinates_from_line(void) {
  char gps_line[200];

  if (line_buffer__remove_line(&line, gps_line, sizeof(gps_line))) {

    // TODO: Parse the line to store GPS coordinates etc.
    // TODO: parse and store to parsed_coordinates
    if (strncmp(gps_line, "$GPGGA", 6) == 0) {
      char *token;
      float latitude = 0;
      float longitude = 0;
      int token_count = 0;

      token = strtok(gps_line, ",");
      while (token != NULL) {
        token = strtok(gps_line, ",");
        token_count++;

        switch (token_count) {
        case 2: // Latitude
          if (token)
            latitude = atof(token) / 100.0;
          // llll.ll -> ll degrees ll.ll minutes
          break;

        case 3:
          if (token && token[0] == 'S')
            latitude = -latitude;
          break;

        case 4: // Longitude
          if (token)
            longitude = atof(token) / 100.0;
          // yyyyy.yy -> yy degrees yy.yy minutes
          break;

        case 5:
          if (token && token[0] == 'W')
            longitude = -longitude;
          break;
        }
      }
      parsed_coordinates.latitude = latitude;
      parsed_coordinates.longitude = longitude;
    }
  }
}

void gps__init(void) {
  line_buffer__init(&line, line_buffer, sizeof(line_buffer));
  uart__init(gps_uart, clock__get_peripheral_clock_hz(), 38400);

  // RX queue should be sized such that can buffer data in UART driver until gps__run_once() is called
  // Note: Assuming 38400bps, we can get 4 chars per ms, and 40 chars per 10ms (100Hz)
  QueueHandle_t rxq_handle = xQueueCreate(50, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(8, sizeof(char)); // We don't send anything to the GPS
  uart__enable_queues(gps_uart, rxq_handle, txq_handle);
}

void gps__run_once(void) {
  gps__transfer_data_from_uart_driver_to_line_buffer();
  gps__parse_coordinates_from_line();
}

gps_coordinates_t gps__get_coordinates(void) {
  // TODO return parsed_coordinates
  return parsed_coordinates;
}