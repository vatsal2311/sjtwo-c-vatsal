#include "line_buffer.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

// Do not access this struct directly in your production code or in unit tests
// These are "internal" details of the code module

/**
 * Initialize *line_buffer_s with the user provided buffer space and size
 * Use should initialize the buffer with whatever memory they need
 * @code
 *  char memory[256];
 *  line_buffer_s line_buffer = { };
 *  line_buffer__init(&line_buffer, memory, sizeof(memory));
 * @endcode
 */

void line_buffer__init(line_buffer_s *buffer, void *memory, size_t size) {
  
  buffer->memory = memory;
  buffer->max_size = size;
  buffer->write_index = 0;
}

// Adds a byte to the buffer, and returns true if the buffer had enough space to add the byte
bool line_buffer__add_byte(line_buffer_s *buffer, char byte) {

  if (buffer->write_index == buffer->max_size) {
    return false;
  }

  ((char *)buffer->memory)[buffer->write_index] = byte;
  buffer->write_index = buffer->write_index + 1;
  return true;
}

bool line_buffer__remove_line(line_buffer_s *buffer, char *line, size_t line_max_size) {

  if (!buffer || !line || line_max_size == 0) {
    return false;
  }

  char *buffer_memory = (char *)buffer->memory;
  char line_size = 0;

  // if buffer is empty hence write_index is 0
  if (buffer->write_index == 0) {
    return false;
  }

  // if buffer has new line character ('\n') and length of line is less than max line size
  uint8_t i;
  for (i = 0; i < buffer->write_index && line_size < line_max_size - 1; ++i) {

    if (buffer_memory[i] == '\n') {
      line[i] = '\0';
      line_size = line_size + 1;
      break;
    }
    line[i] = buffer_memory[i];
    line_size = line_size + 1;
  }

  // if the buffer doesn't have new line character
  if (i == buffer->write_index) {
    memcpy(line, buffer_memory, i);
    return false;
  }

  // if line_size is equal to line max size
  if (line_size == line_max_size) {
    memcpy(line, buffer_memory, line_size);
  }
  
  //Shift the remaining buffer ahead for the count
  uint8_t remaining_buffer = buffer->write_index - i - 1;
  uint8_t j;
  for (j = 0; j < remaining_buffer; j++) {
    buffer_memory[j] = buffer_memory[j + i + 1];
  }

  // Update of wrtie index
  buffer->write_index = remaining_buffer;

  return true;
}