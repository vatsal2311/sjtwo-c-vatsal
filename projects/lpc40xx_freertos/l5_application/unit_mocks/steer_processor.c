#include "steering.h"
#include <stdint.h>

#define THRESHOLD 50

void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm) {

  if (left_sensor_cm < THRESHOLD && right_sensor_cm < THRESHOLD) {

    if (right_sensor_cm < left_sensor_cm) {
      steer_left();
    } else
      steer_right();
  } else if (right_sensor_cm < THRESHOLD && left_sensor_cm > THRESHOLD) {
    steer_left();
  } else if (left_sensor_cm < THRESHOLD && right_sensor_cm > THRESHOLD) {
    steer_right();
  }
}
