#include "message_processor.h"
#include "message.h"

static bool checker(message_s message, bool symbol_found) {

  if ((message.data[0] == '$') || (message.data[0] == '#')) {
    symbol_found = true;
  } else {
    symbol_found = false;
  }
  return symbol_found;
}

bool message_processor(void) {
  message_s message;
  bool symbol_found = false;
  memset(&message, 0, sizeof(message));

  const static size_t max_messages_to_process = 3;
  for (size_t message_count = 0; message_count < max_messages_to_process; message_count++) {
    if (!message__read(&message)) {
      break;
    } else {
      symbol_found = checker(message, symbol_found);
    }
  }
  return symbol_found;
}