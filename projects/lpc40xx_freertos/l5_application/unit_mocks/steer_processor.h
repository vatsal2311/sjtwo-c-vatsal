#pragma once

#include <stdint.h>

#include "steering.h"

void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm);