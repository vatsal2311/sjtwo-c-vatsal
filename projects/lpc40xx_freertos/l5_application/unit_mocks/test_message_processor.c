#include "Mockmessage.h"
#include "message_processor.h"
#include "unity.h"

static bool message__read_stub(message_s *message_to_read, int call_count) {
  bool message_was_read = false;

  if (call_count > 2) {
    message_was_read = false;
  } else {
    message_was_read = true;
  }

  if (call_count == 0) {
    message_to_read->data[0] = '$';
  }
  if (call_count == 1) {
    message_to_read->data[0] = '$';
  }
  if (call_count == 2) {
    message_to_read->data[0] = '$';
  }

  return message_was_read;
}

// This only tests if we process at most 3 messages
void test_process_3_messages(void) {
  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  // Third time when message_read() is called, we will return false to break the loop
  message__read_ExpectAndReturn(NULL, false);
  message__read_IgnoreArg_message_to_read();

  // Since we did not return a message that starts with '$' this should return false
  TEST_ASSERT_FALSE(message_processor());
}

void test_process_message_with_dollar_sign(void) {

  // Test case is for '$', so message_processor() returning TRUE. Hence the assert case is also TRUE
  message__read_StubWithCallback(message__read_stub);
  TEST_ASSERT_TRUE(message_processor())
}

void test_process_messages_without_any_dollar_sign(void) {

  // Test case is for without '$', so message_processor() returning FALSE. Hence the assert case is being TRUE
  message__read_StubWithCallback(message__read_stub);
  TEST_ASSERT_FALSE(!message_processor())
}

void test_process_messages_with_stubWithCallback(void) {

  message__read_StubWithCallback(message__read_stub);
  // Function under test
  message_processor();
}
