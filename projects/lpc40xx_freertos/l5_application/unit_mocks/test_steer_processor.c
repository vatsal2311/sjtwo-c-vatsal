#include "Mocksteering.h"
#include "steer_processor.h"
#include "unity.h"

void test_steer_processor__move_left(void) {

  steer_left_Expect();
  steer_processor(55, 39);
}
void test_steer_processor__move_right(void) {

  steer_right_Expect();
  steer_processor(39, 65);
}
void test_steer_processor__both_sensors_less_than_threshold(void) {
  steer_right_Expect();
  steer_processor(28, 39);
}
void test_steer_processor__both_sensors_more_than_threshold(void) { steer_processor(56, 65); }

void test_steer_processor(void) {
  steer_right_Expect();
  steer_processor(10, 20);

  steer_left_Expect();
  steer_processor(20, 10);
}